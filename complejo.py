
"""Programa Complejo"""
class Complejo:
    def __init__(self,a,b):
        self.parte_real=a
        self.parte_compleja=b
    
    def __str__(self):
        return str(self.parte_real) +  "+" + str(self.parte_compleja) + "i"
    """static method"""
    def sumar(c_1,c_2):
        suma_real= c_1.parte_real+c_2.parte_real
        suma_compleja=c_1.parte_compleja+c_2.parte_compleja
        resultado= Complejo(suma_real,suma_compleja)
        return resultado
    def sumame(self,c_2):#como hacerlo para programacion oop
        suma_real= self.parte_real+c_2.parte_real
        suma_compleja=self.parte_compleja+c_2.parte_compleja
        resultado= Complejo(suma_real,suma_compleja)
        return resultado
    def restame(self,c_2):
        resta_real= self.parte_real-c_2.parte_real
        resta_compleja=self.parte_compleja-c_2.parte_compleja
        resultado=Complejo(resta_real,resta_compleja)
        return resultado

    def producto(self, c_2):
        real= self.parte_real*c_2.parte_real
        con_i= self.parte_real*c_2.parte_compleja
        con_i2= self.parte_compleja*c_2.parte_real
        real2= (self.parte_compleja*c_2.parte_compleja)*(-1)
        r=(real+real2)
        i=(con_i + con_i2)
        producto=Complejo(r, i)
        return producto

    def conjugado(self):
        a=self.parte_real
        b=(self.parte_compleja)*(-1)
        conjugado=Complejo(a, b)
        return conjugado

    #def divi(self, c_2):
        #num=self
        #denom=c_2
        #u=num.producto(denom.conjugado())
        #d=denom.producto(denom.conjugado())
        #real=u.parte_real/d
        #imaginaria=u.parte_compleja/d
        #resultado=Complejo(real, imaginaria)
        #return resultado


complejo_1=Complejo(1,-2)
complejo_2=Complejo(2,5)
c_3=complejo_1.sumame(complejo_2)
c_4=complejo_2.restame(complejo_1)
c_5=complejo_1.producto(complejo_2)
#c_6=complejo_1.divi(complejo_2)
c_7=complejo_1.conjugado()
print(c_3,c_4,c_5,c_7)