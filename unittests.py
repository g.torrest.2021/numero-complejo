import unittest
from complejo import Complejo

class TestComplejo(unittest.TestCase):

    def test_construir(self):
        e1 = Complejo(1, -2)
        self.assertEqual(e1.parte_real,1)
        self.assertEqual(e1.parte_compleja,-2)
    def test_suma(self):
        e1=Complejo(1,-2)
        e2=Complejo(2,4)
        self.assertNotEqual(e1.sumame(e2),Complejo(3,3))

if __name__ == "__main__":
    unittest.main()

